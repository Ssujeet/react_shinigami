import React from 'react';
import { FaHeart, FaRegComment, FaRegPaperPlane } from 'react-icons/fa'
export default class Card extends React.Component {
    state = {
        title: ""
    }
    render() {
        return (
            <div class="col-md-6 mt-5" style={{ position: "relative", fontSize: 16 }}>
                <div class="card" >
                    <div style={{ padding: 10 }}>
                        <img src={require("../images/round.png")} alt="..." class="rounded-circle col-2 col-sm-2 col-md-1 " />
                        <span style={{ color: "black" }}>
                            amonlama
                        </span>
                    </div>
                    <img src={require("../images/image.png")} class="card-img-top" alt="..." />
                    <div class="card-body">
                        <div class="col-6  col-sm-3 col-md-3" style={{ display: "flex", justifyContent: "space-between", paddingLeft: 0 }}>
                            <FaHeart color="red" size={30} />
                            <FaRegComment color="black" size={30} />
                            <FaRegPaperPlane color="black" size={30} />
                        </div>
                        <div class="mt-3">
                            <p style={{ color: 'black' }}><strong>
                                7 likes
                            </strong></p>
                            <span style={{ color: 'black', marginRight: 5 }}><strong>
                                angilajoli
                            </strong></span>
                            <span style={{ color: 'black' }}><strong>
                                hey nice mobile
                            </strong></span>
                            <p style={{ color: "#9f9c9c" }}>
                                4 days ago
                            </p>
                            <form style={{ flexDirection: "row", display: "flex" }}>
                                <input class="form-control" type="text" placeholder="Add a Comment"
                                    value={this.state.value}
                                    onChange={(value) => this.setState({ title: value })}
                                />
                                <input
                                    type="submit"
                                    value="Post"
                                />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
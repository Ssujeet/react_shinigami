import React from 'react';
import { FaHeart, FaRegComment, FaRegPaperPlane } from 'react-icons/fa'
import StoryContent from './StoryContent'
export default class StoriesBox extends React.Component {
    render() {
        return (

            <div class="card" style={{ position: "relative", fontSize: 16, padding: 5 }}>
                <div style={{ color: "black", display: "flex", justifyContent: "space-between", flexDirection: "row", fontSize: 16 }}>

                    <p>
                        Stories
                    </p>
                    <p>
                        Watch All
                    </p>

                </div>
                <StoryContent />
            </div>


        )
    }

}
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Home from '../Pages/Home/App'
import Discover from '../Pages/Discover/index'
import Notification from '../Pages/Notification/index'
import Profile from '../Pages/Profile/index'

export default class Navigation extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                        <a class="navbar-brand" href="#">Shinigami</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse container " id="navbarCollapse">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <Link class="nav-link active " to="/">Home</Link></li>
                                <li class="nav-item">
                                    <Link class="nav-link active " to="/discover">Discover</Link>
                                </li>
                                <li class="nav-item">
                                    <Link class="nav-link active " to="/notification">Notification</Link>  </li>
                                <li><Link class="nav-link active" to="/profile">Profile</Link></li>

                            </ul>
                            <form class="form-inline mt-2 mt-md-0">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </nav>

                    {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                    <Switch>
                        <Route path="/discover">
                            <Discover />
                        </Route>
                        <Route path="/notification">
                            <Notification />
                        </Route>
                        <Route path="/profile">
                            <Profile />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </div>
            </Router >
        );
    }
}
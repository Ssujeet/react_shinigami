import React from 'react';
import { FaHeart, FaRegComment, FaRegPaperPlane } from 'react-icons/fa'
export default class FollowBox extends React.Component {
    render() {
        return (

            <div class="col-4 col-sm-4 col-md-4 " style={{ position: "relative", fontSize: 16 }}>
                <div class="card" >
                    <div style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}>
                        <img src={require("../images/round.png")} alt="..." class="rounded-circle col-10 col-sm-5 col-md-6 mt-3" />
                    </div>
                    <div class="card-body" style={{ margin: "auto" }}>
                        <p >
                            Sujeet Singh
                        </p>
                        <p style={{ color: "#98a3b4" }} >
                            Follows you
                        </p>
                        <button type="button" class="btn btn-primary btn-lg">Follow</button>                    </div>
                </div>
            </div>


        )
    }

}
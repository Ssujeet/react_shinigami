import React from 'react';
import FollowBox from '../Components/FollowBox'
export default class SuggestionBox extends React.Component {
    render() {
        return (
            <div className="card col-md-6 mt-5 pt-3 pb-3" style={{ color: "black" }}>
                <div style={{ color: "black", display: "flex", justifyContent: "space-between", flexDirection: "row", fontSize: 16 }}>

                    <p>
                        Suggestions for you
                    </p>
                    <p>
                        See All
                    </p>
                </div>
                <div className="row">
                    <FollowBox />
                    <FollowBox />
                    <FollowBox />
                </div>

            </div>

        )
    }

}
import React from 'react';
import './App.css'
import Card from '../../Components/Card'
import SuggestionBox from '../../Components/SuggestionBox'
import StoriesBox from '../../Components/StoriesBox'
function App() {
  return (
    <div style={{ position: "relative", marginTop: 80, marginBottom: 80 }}>
      <header className="App-header">
        <Card />
        <Card />
        <SuggestionBox />
        <Card /><Card />

        <StoriesBox />
      </header>
    </div>

  );
}

export default App;
